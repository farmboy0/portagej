package portage;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Seq;

public class DependencyParser {

	public Seq<Dependency> parse(@Nonnull String deps) {
		Seq<String> token = API.Seq(deps.split(" "));
		State state = new State(token);
		parse(state);
		return state.dependencies;
	}

	private void parse(final State state) {
		State substate = new State(state.token);
		while (!state.token.isEmpty() || ")".equals(substate.token.get(0))) {
			parseNextDependency(substate);
			state.dependencies = state.dependencies.appendAll(substate.dependencies);
			substate = new State(substate.resultToken);
		}
		state.resultToken = substate.token;
	}

	private void parseNextDependency(final State state) {
		final String firstToken = state.token.get(0);
		if (firstToken.endsWith("?")) {
			if (state.token.size() < 4 || !"(".equals(state.token.get(1))) {
				throw new IllegalArgumentException("invalid conditional " + state.token.mkString(" "));
			}
			final State substate = new State(state.token.subSequence(2));
			parse(substate);
			state.dependencies = state.dependencies.append(new IUseDependency(null, firstToken, substate.dependencies));
			state.resultToken = substate.resultToken.subSequence(1);
		} else if ("||".equals(firstToken)) {
			if (state.token.size() < 5 || !"(".equals(state.token.get(1))) {
				throw new IllegalArgumentException("invalid conditional " + state.token.mkString(" "));
			}
			final State substate = new State(state.token.subSequence(2));
			parse(substate);
			state.dependencies = state.dependencies.append(new OrDependency(substate.dependencies));
			state.resultToken = substate.resultToken.subSequence(1);
		} else {
			state.dependencies = state.dependencies.append(new Atom(firstToken));
			state.resultToken = state.token.subSequence(1);
		}
	}

	private static final class State {
		public final Seq<String> token;

		public Seq<Dependency> dependencies = API.Seq();
		public Seq<String> resultToken = API.Seq();

		public State(Seq<String> token) {
			this.token = token;
		}
	}
}
