package portage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import javax.annotation.Nonnull;

import io.vavr.API;
import io.vavr.collection.Seq;
import io.vavr.collection.Set;
import io.vavr.collection.SortedSet;

import org.ini4j.Profile.Section;
import org.ini4j.Wini;

public class RepositoryExplorer {
	private Seq<Repository> repos = API.Seq();

	public RepositoryExplorer() throws IOException {
		init();
	}

	private void init() throws IOException {
		final File reposConf = new File("/etc/portage/repos.conf");
		if (!reposConf.exists() || !reposConf.isDirectory() || !reposConf.canRead()) {
			throw new IOException("can't read repository configs in /etc/portage/repos.conf");
		}
		String mainRepo = null;
		for (File file : reposConf.listFiles((dir, name) -> name.endsWith(".conf"))) {
			Wini wini = new Wini(file);
			for (String sectionName : wini.keySet()) {
				Section section = wini.get(sectionName);
				if (sectionName.equals("DEFAULT")) {
					mainRepo = section.get("main-repo");
				} else {
					String location = section.get("location");
					Repository repo = new Repository(sectionName, new File(location), sectionName.equals(mainRepo));
					repos = repos.append(repo);
				}
			}
		}
	}

	@Nonnull
	public SortedSet<String> getCategories() {
		return repos.map(Repository::location)
			.flatMap(location -> API.Seq(location.listFiles(RepositoryExplorer::isCategoryDir)))
			.map(File::getName)
			.toSortedSet();
	}

	public SortedSet<String> getPackagenames(@Nonnull String category) {
		return repos.map(Repository::location)
			.flatMap(location -> API.Seq(location.listFiles(RepositoryExplorer::isCategoryDir)))
			.filter(catDir -> Objects.equals(catDir.getName(), category))
			.flatMap(location -> API.Seq(location.listFiles(File::isDirectory)))
			.map(File::getName)
			.toSortedSet();
	}

	private static final Set<String> SPECIAL_CATEGORY_DIRS = API.Set("distfiles", "eclass", "licenses", "metadata",
		"packages", "profiles", "scripts");

	private static boolean isCategoryDir(File file) {
		return file.isDirectory() && file.canRead() && !SPECIAL_CATEGORY_DIRS.contains(file.getName())
			&& !file.getName().startsWith(".") && file.list().length != 0;
	}

	private record Repository(String name, File location, boolean mainRepo) {
	}
}
