package portage;

import static io.vavr.API.LinkedMap;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import io.vavr.API;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;

public class ProfileExplorer {
	private Map<ProfilePath, Seq<ProfilePath>> profilePaths = LinkedMap();

	public ProfileExplorer() throws IOException {
		File makeProfile = new File("/etc/portage/make.profile");
		if (!makeProfile.exists()) {
			return;
		}
		if (!makeProfile.isDirectory() || !makeProfile.canRead()) {
			throw new IOException("cant access " + makeProfile + ", not a directory or not readable");
		}
		resolveParents(new ProfilePath(makeProfile.toPath().toRealPath()));
	}

	private static final Path PARENT = Path.of("parent");

	private void resolveParents(ProfilePath path) throws IOException {
		final Path parentPath = path.profilePath().resolve(PARENT);
		if (!parentPath.toFile().exists()) {
			return;
		}
		Seq<ProfilePath> parents = API.Seq();
		for (String parentEntry : Files.readAllLines(parentPath)) {
			parents = parents.append(new ProfilePath(path.profilePath().resolve(parentEntry).toRealPath()));
		}
		profilePaths = profilePaths.put(path, parents);
		for (ProfilePath parent : parents) {
			resolveParents(parent);
		}
	}

	private static record ProfilePath(Path profilePath) {

	}
}
