package portage;

import io.vavr.collection.Seq;

public class OrDependency implements Dependency {
	private final Seq<Dependency> dependencies;

	public OrDependency(Seq<Dependency> dependencies) {
		this.dependencies = dependencies;
	}
}
