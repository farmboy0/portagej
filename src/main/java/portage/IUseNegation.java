package portage;

public enum IUseNegation {
	NONE, ONCE, TWICE;
}
