package portage;

import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nonnull;

/**
 * Describes a filter that can match more than one package.
 */
public class Atom implements Dependency {
	private static final String PATTERN = "^(>|>=|=|<=|<|~|!|!!)?([a-z-*]+)/([a-zA-Z0-9*]+)(-([_a-z0-9\\.]+)(-([a-z0-9]+))?)?$";

	private String prefix;

	private String category;

	private String packageName;

	private String packageVersion;

	private String packageRevision;

	public Atom(@Nonnull String atom) throws IllegalArgumentException {
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(atom);
		if (!matcher.matches()) {
			throw new IllegalArgumentException(atom + " is not a valid atom string");
		}
		MatchResult result = matcher.toMatchResult();
		for (int i = 1; i <= result.groupCount(); i++) {
			String value = result.group(i);
			if (i == 1) {
				prefix = value;
			} else if (i == 2) {
				category = value;
			} else if (i == 3) {
				packageName = value;
			} else if (i == 5) {
				packageVersion = value;
			} else if (i == 7) {
				packageRevision = value;
			}
		}
	}

	public String getPrefix() {
		return prefix;
	}

	public String getCategory() {
		return category;
	}

	public String getPackageName() {
		return packageName;
	}

	public String getPackageVersion() {
		return packageVersion;
	}

	public String getPackageRevision() {
		return packageRevision;
	}
}
