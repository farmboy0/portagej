package portage;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Nonnull;

public class EBuildPhaseLauncher {
	private final ExecutorService exec;

	public EBuildPhaseLauncher() {
		exec = Executors.newFixedThreadPool(12, r -> new Thread(r, "PhaseLaunch"));
	}

	public Future<Properties> launch(@Nonnull File ebuild, @Nonnull EbuildPhase phase) {
		return exec.submit(() -> {
			final ProcessBuilder pb = new ProcessBuilder("/usr/lib/portage/python3.9/ebuild.sh");
			pb.environment().put("PORTAGE_BIN_PATH", "/usr/lib/portage/python3.9");
			pb.environment().put("EBUILD", ebuild.getAbsolutePath());
			pb.environment().put("EBUILD_PHASE", phase.getPhaseName());
			pb.environment().put("PORTAGE_ECLASS_LOCATIONS", "/usr/portage");
			pb.environment().put("PORTAGE_PIPE_FD", "1");
			final Process p = pb.start();
			final boolean exited = p.waitFor(10, SECONDS);

			final Properties result = new Properties();
			if (exited) {
				result.load(p.inputReader());
			}
			return result;
		});
	}

	private static final class PhaseRun implements Runnable {
		public PhaseRun() {
		}

		@Override
		public void run() {
		}
	}
}
