package portage;

import java.util.Properties;

import javax.annotation.Nonnull;

public class EBuildProperties {
	private final Properties properties;

	public EBuildProperties(@Nonnull Properties properties) {
		this.properties = properties;
	}

	public String getEAPI() {
		return properties.getProperty("EAPI", "-1");
	}

	private void parseIUSE() {

	}
}
