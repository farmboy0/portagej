package portage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ConfigurationExplorer {
	private static final Path MAKE_CONF = Paths.get("/etc/portage/make.conf");

	public String getMakeConf() throws IOException {
		return Files.readString(MAKE_CONF);
	}
}
