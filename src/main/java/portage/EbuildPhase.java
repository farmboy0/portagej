package portage;

public enum EbuildPhase {
	DEPEND("depend");

	private String phaseName;

	private EbuildPhase(String phaseName) {
		this.phaseName = phaseName;
	}

	public String getPhaseName() {
		return phaseName;
	}
}
