package portage;

import io.vavr.collection.Seq;

public class IUseDependency implements Dependency {
	private final IUseNegation negation;
	private final String use;
	private final Seq<Dependency> dependencies;

	public IUseDependency(IUseNegation negation, String use, Seq<Dependency> dependencies) {
		this.negation = negation;
		this.use = use;
		this.dependencies = dependencies;
	}

	public IUseNegation getNegation() {
		return negation;
	}

	public String getUse() {
		return use;
	}

	public Seq<Dependency> getDependencies() {
		return dependencies;
	}
}
