package ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.JTree;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableModel;
import javax.swing.tree.TreeModel;

public class PortageView {

	private JFrame frame;
	private JMenuBar menuBar;
	private JMenu actionsMenu;
	private JMenuItem quitAction;
	private JToolBar toolBar;
	private JTabbedPane tabbedPane;
	private JSplitPane packagesSplit;
	private JPanel panel;
	private JTextField search;
	private JScrollPane scrollPane;
	private JTree packagesTree;
	private JScrollPane makeConfScroll;
	private JTextArea makeConf;
	private JScrollPane scrollPane_2;
	private JPanel infoPanel;
	private JScrollPane useflagsScroll;
	private JTable useTable;
	private JPanel masking;
	private JTextArea textArea;
	private JTextArea textArea_1;
	private JTextArea textArea_2;
	private JTextArea textArea_3;
	private JPanel environment;
	private JPanel envComboPanel;
	private JTextArea envTextarea;
	private JComboBox environmentCombo;
	private JButton envAddButton;
	private JButton envRemoveButton;
	private JLabel packagename;
	private JLabel homepage;
	private JLabel description;
	private JLabel licenses;
	private JTable packageversions;
	private JTable packageuses;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					PortageView window = new PortageView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PortageView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setPreferredSize(new Dimension(1024, 800));
		frame.setMinimumSize(new Dimension(640, 480));
		frame.setTitle("PortageJ");
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setJMenuBar(getMenuBar());
		frame.getContentPane().add(getToolBar(), BorderLayout.NORTH);
		frame.getContentPane().add(getTabbedPane(), BorderLayout.CENTER);
	}

	public void show() {
		frame.setVisible(true);
	}

	private JMenuBar getMenuBar() {
		if (menuBar == null) {
			menuBar = new JMenuBar();
			menuBar.add(getActionsMenu());
		}
		return menuBar;
	}

	private JMenu getActionsMenu() {
		if (actionsMenu == null) {
			actionsMenu = new JMenu("Actions");
			actionsMenu.add(getQuitAction());
		}
		return actionsMenu;
	}

	private JMenuItem getQuitAction() {
		if (quitAction == null) {
			quitAction = new JMenuItem("Quit");
			quitAction.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			});
		}
		return quitAction;
	}

	private JToolBar getToolBar() {
		if (toolBar == null) {
			toolBar = new JToolBar();
		}
		return toolBar;
	}

	private JTabbedPane getTabbedPane() {
		if (tabbedPane == null) {
			tabbedPane = new JTabbedPane(JTabbedPane.TOP);
			tabbedPane.addTab("Packages", null, getPackagesSplit(), null);
			tabbedPane.addTab("make.conf", null, getMakeConfScroll(), null);
			tabbedPane.addTab("Use flags", null, getUseflagsScroll(), null);
			tabbedPane.addTab("Masking", null, getMasking(), null);
			tabbedPane.addTab("Environment", null, getEnvironment(), null);
		}
		return tabbedPane;
	}

	private JSplitPane getPackagesSplit() {
		if (packagesSplit == null) {
			packagesSplit = new JSplitPane();
			packagesSplit.setLeftComponent(getPanel());
			packagesSplit.setRightComponent(getScrollPane_2());
		}
		return packagesSplit;
	}

	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(new BorderLayout(0, 0));
			panel.add(getSearch(), BorderLayout.NORTH);
			panel.add(getScrollPane(), BorderLayout.CENTER);
		}
		return panel;
	}

	private JTextField getSearch() {
		if (search == null) {
			search = new JTextField();
			search.setColumns(10);
		}
		return search;
	}

	private JScrollPane getScrollPane() {
		if (scrollPane == null) {
			scrollPane = new JScrollPane();
			scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			scrollPane.setViewportView(getPackagesTree());
		}
		return scrollPane;
	}

	private JTree getPackagesTree() {
		if (packagesTree == null) {
			packagesTree = new JTree();
			packagesTree.setToggleClickCount(1);
			packagesTree.setShowsRootHandles(false);
			packagesTree.setRootVisible(false);
		}
		return packagesTree;
	}

	public TreeModel getTreeModel() {
		return getPackagesTree().getModel();
	}

	public void setTreeModel(TreeModel model) {
		getPackagesTree().setModel(model);
	}

	private JScrollPane getMakeConfScroll() {
		if (makeConfScroll == null) {
			makeConfScroll = new JScrollPane();
			makeConfScroll.setViewportView(getMakeConf());
		}
		return makeConfScroll;
	}

	private JTextArea getMakeConf() {
		if (makeConf == null) {
			makeConf = new JTextArea();
			makeConf.setFont(new Font("Liberation Mono", Font.PLAIN, 14));
			makeConf.setEditable(false);
		}
		return makeConf;
	}

	private JScrollPane getScrollPane_2() {
		if (scrollPane_2 == null) {
			scrollPane_2 = new JScrollPane();
			scrollPane_2.setViewportView(getInfoPanel());
		}
		return scrollPane_2;
	}

	private JPanel getInfoPanel() {
		if (infoPanel == null) {
			infoPanel = new JPanel();
			GridBagLayout gblInfoPanel = new GridBagLayout();
			infoPanel.setLayout(gblInfoPanel);
			GridBagConstraints gbcPackagename = new GridBagConstraints();
			gbcPackagename.fill = GridBagConstraints.VERTICAL;
			gbcPackagename.weightx = 1.0;
			gbcPackagename.gridheight = 2;
			gbcPackagename.anchor = GridBagConstraints.NORTH;
			gbcPackagename.insets = new Insets(0, 0, 0, 5);
			gbcPackagename.gridx = 0;
			gbcPackagename.gridy = 0;
			infoPanel.add(getPackagename(), gbcPackagename);
			GridBagConstraints gbcHomepage = new GridBagConstraints();
			gbcHomepage.weightx = 1.0;
			gbcHomepage.insets = new Insets(0, 0, 0, 5);
			gbcHomepage.gridx = 0;
			gbcHomepage.gridy = 2;
			infoPanel.add(getHomepage(), gbcHomepage);
			GridBagConstraints gbcDescription = new GridBagConstraints();
			gbcDescription.weightx = 1.0;
			gbcDescription.fill = GridBagConstraints.HORIZONTAL;
			gbcDescription.anchor = GridBagConstraints.WEST;
			gbcDescription.insets = new Insets(0, 0, 0, 5);
			gbcDescription.gridx = 0;
			gbcDescription.gridy = 3;
			infoPanel.add(getDescription(), gbcDescription);
			GridBagConstraints gbcLicenses = new GridBagConstraints();
			gbcLicenses.weightx = 1.0;
			gbcLicenses.fill = GridBagConstraints.HORIZONTAL;
			gbcLicenses.anchor = GridBagConstraints.WEST;
			gbcLicenses.insets = new Insets(0, 0, 0, 5);
			gbcLicenses.gridx = 0;
			gbcLicenses.gridy = 4;
			infoPanel.add(getLicenses(), gbcLicenses);
			GridBagConstraints gbcPackageversions = new GridBagConstraints();
			gbcPackageversions.weighty = 0.5;
			gbcPackageversions.weightx = 1.0;
			gbcPackageversions.fill = GridBagConstraints.HORIZONTAL;
			gbcPackageversions.gridheight = 4;
			gbcPackageversions.gridx = 0;
			gbcPackageversions.gridy = 5;
			infoPanel.add(getPackageversions(), gbcPackageversions);
			GridBagConstraints gbcPackageuses = new GridBagConstraints();
			gbcPackageuses.weighty = 0.5;
			gbcPackageuses.weightx = 1.0;
			gbcPackageuses.fill = GridBagConstraints.HORIZONTAL;
			gbcPackageuses.gridheight = 4;
			gbcPackageuses.gridx = 0;
			gbcPackageuses.gridy = 10;
			infoPanel.add(getPackageuses(), gbcPackageuses);
		}
		return infoPanel;
	}

	private JScrollPane getUseflagsScroll() {
		if (useflagsScroll == null) {
			useflagsScroll = new JScrollPane();
			useflagsScroll.setViewportView(getUseTable());
		}
		return useflagsScroll;
	}

	private JTable getUseTable() {
		if (useTable == null) {
			useTable = new JTable();
		}
		return useTable;
	}

	private JPanel getMasking() {
		if (masking == null) {
			masking = new JPanel();
			masking.setLayout(new GridLayout(2, 2, 10, 10));
			masking.add(getTextArea());
			masking.add(getTextArea_1());
			masking.add(getTextArea_2());
			masking.add(getTextArea_3());
		}
		return masking;
	}

	private JTextArea getTextArea() {
		if (textArea == null) {
			textArea = new JTextArea();
			textArea.setEditable(false);
		}
		return textArea;
	}

	private JTextArea getTextArea_1() {
		if (textArea_1 == null) {
			textArea_1 = new JTextArea();
			textArea_1.setEditable(false);
		}
		return textArea_1;
	}

	private JTextArea getTextArea_2() {
		if (textArea_2 == null) {
			textArea_2 = new JTextArea();
			textArea_2.setEditable(false);
		}
		return textArea_2;
	}

	private JTextArea getTextArea_3() {
		if (textArea_3 == null) {
			textArea_3 = new JTextArea();
			textArea_3.setEditable(false);
		}
		return textArea_3;
	}

	private JPanel getEnvironment() {
		if (environment == null) {
			environment = new JPanel();
			environment.setLayout(new BorderLayout(0, 0));
			environment.add(getEnvComboPanel(), BorderLayout.NORTH);
			environment.add(getEnvTextarea(), BorderLayout.CENTER);
		}
		return environment;
	}

	private JPanel getEnvComboPanel() {
		if (envComboPanel == null) {
			envComboPanel = new JPanel();
			envComboPanel.setLayout(new BoxLayout(envComboPanel, BoxLayout.X_AXIS));
			envComboPanel.add(getEnvAddButton());
			envComboPanel.add(getEnvironmentCombo());
			envComboPanel.add(getEnvRemoveButton());
		}
		return envComboPanel;
	}

	private JTextArea getEnvTextarea() {
		if (envTextarea == null) {
			envTextarea = new JTextArea();
			envTextarea.setEditable(false);
		}
		return envTextarea;
	}

	private JComboBox getEnvironmentCombo() {
		if (environmentCombo == null) {
			environmentCombo = new JComboBox();
		}
		return environmentCombo;
	}

	private JButton getEnvAddButton() {
		if (envAddButton == null) {
			envAddButton = new JButton("+");
		}
		return envAddButton;
	}

	private JButton getEnvRemoveButton() {
		if (envRemoveButton == null) {
			envRemoveButton = new JButton("-");
		}
		return envRemoveButton;
	}

	public String getMakeConfText() {
		return getMakeConf().getText();
	}

	public void setMakeConfText(String text) {
		getMakeConf().setText(text);
	}

	private JLabel getPackagename() {
		if (packagename == null) {
			packagename = new JLabel("Packagename");
			packagename.setFont(new Font("Liberation Sans", Font.BOLD, 26));
		}
		return packagename;
	}

	private JLabel getHomepage() {
		if (homepage == null) {
			homepage = new JLabel("Homepage");
		}
		return homepage;
	}

	private JLabel getDescription() {
		if (description == null) {
			description = new JLabel("Description");
		}
		return description;
	}

	private JLabel getLicenses() {
		if (licenses == null) {
			licenses = new JLabel("Licenses");
		}
		return licenses;
	}

	private JTable getPackageversions() {
		if (packageversions == null) {
			packageversions = new JTable();
		}
		return packageversions;
	}

	private JTable getPackageuses() {
		if (packageuses == null) {
			packageuses = new JTable();
		}
		return packageuses;
	}

	public String getPackagenameText() {
		return getPackagename().getText();
	}

	public void setPackagenameText(String text_1) {
		getPackagename().setText(text_1);
	}

	public String getHomepageText() {
		return getHomepage().getText();
	}

	public void setHomepageText(String text_2) {
		getHomepage().setText(text_2);
	}

	public String getDescriptionText() {
		return getDescription().getText();
	}

	public void setDescriptionText(String text_3) {
		getDescription().setText(text_3);
	}

	public String getLicensesText() {
		return getLicenses().getText();
	}

	public void setLicensesText(String text_4) {
		getLicenses().setText(text_4);
	}

	public TableModel getPackageversionsModel() {
		return getPackageversions().getModel();
	}

	public void setPackageversionsModel(TableModel model_1) {
		getPackageversions().setModel(model_1);
	}

	public TableModel getPackageusesModel() {
		return getPackageuses().getModel();
	}

	public void setPackageusesModel(TableModel model_2) {
		getPackageuses().setModel(model_2);
	}
}
