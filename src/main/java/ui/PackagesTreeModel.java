package ui;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import io.vavr.API;
import io.vavr.collection.Map;
import io.vavr.collection.Seq;

import portage.RepositoryExplorer;

public class PackagesTreeModel implements TreeModel {

	private final RepositoryExplorer repos;

	public PackagesTreeModel(RepositoryExplorer repos) {
		this.repos = repos;
	}

	private Seq<Category> categories = null;

	private Seq<Category> getCategories() {
		if (categories == null) {
			categories = repos.getCategories().toArray().map(Category::new);
		}
		return categories;
	}

	private Map<Category, Seq<PackageName>> packageNames = API.Map();

	private Seq<PackageName> getPackageNames(Category category) {
		if (!packageNames.containsKey(category)) {
			packageNames = packageNames.put(category,
				repos.getPackagenames(category.name()).toArray().map(PackageName::new));
		}
		return packageNames.get(category).get();
	}

	@Override
	public Object getRoot() {
		return repos;
	}

	@Override
	public Object getChild(Object parent, int index) {
		if (parent == repos) {
			return getCategories().get(index);
		}
		if (parent instanceof Category category) {
			return getPackageNames(category).get(index);
		}
		return null;
	}

	@Override
	public int getChildCount(Object parent) {
		if (parent == repos) {
			return getCategories().size();
		}
		if (parent instanceof Category category) {
			return getPackageNames(category).size();
		}
		return 0;
	}

	@Override
	public boolean isLeaf(Object node) {
		if (node == repos) {
			return false;
		}
		if (node instanceof Category) {
			return false;
		}
		return true;
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue) {
	}

	@Override
	public int getIndexOfChild(Object parent, Object child) {
		if (parent == repos) {
			return getCategories().indexOf((Category) child);
		}
		if (parent instanceof Category category) {
			return getPackageNames(category).indexOf((PackageName) child);
		}
		return 0;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l) {
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l) {
	}

	private final record Category(String name) {
		@Override
		public String toString() {
			return name;
		}
	}

	private final record PackageName(String name) {
		@Override
		public String toString() {
			return name;
		}
	}

	private final record Version(String version) {
		@Override
		public String toString() {
			return version;
		}
	}
}
