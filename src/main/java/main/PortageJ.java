package main;

import java.awt.EventQueue;
import java.io.IOException;

import javax.swing.UIManager;

import portage.ConfigurationExplorer;
import portage.RepositoryExplorer;
import ui.PackagesTreeModel;
import ui.PortageView;

public class PortageJ {

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		} catch (Exception e) {
			e.printStackTrace(System.err);
		}

		EventQueue.invokeLater(() -> {
			try {
				ConfigurationExplorer ce = new ConfigurationExplorer();
				RepositoryExplorer re = new RepositoryExplorer();
				PackagesTreeModel treeModel = new PackagesTreeModel(re);
				PortageView ui = new PortageView();
				ui.setTreeModel(treeModel);
				ui.setMakeConfText(ce.getMakeConf());
				ui.show();
			} catch (IOException e) {
				e.printStackTrace(System.err);
			}
		});
	}

}
