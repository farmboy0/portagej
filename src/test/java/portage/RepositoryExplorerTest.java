package portage;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;

import io.vavr.collection.SortedSet;

import org.junit.jupiter.api.Test;

class RepositoryExplorerTest {

	@Test
	void testGetCategories() throws IOException {
		RepositoryExplorer re = new RepositoryExplorer();
		SortedSet<String> categories = re.getCategories();
		categories.forEach(System.out::println);
		assertNotEquals(0, categories.size());
	}

}
