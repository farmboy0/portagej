package portage;

import static org.junit.jupiter.api.Assertions.assertFalse;

import java.io.File;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import org.junit.jupiter.api.Test;

class EBuildPhaseLauncherTest {

	@Test
	void testLaunch() throws Exception {
		EBuildPhaseLauncher launcher = new EBuildPhaseLauncher();
		File test = new File("/mnt/work/Repositories/portage-overlay/dev-util/apitrace/apitrace-9999.ebuild");
		Future<Properties> f = launcher.launch(test, EbuildPhase.DEPEND);
		Properties p = f.get();
		assertFalse(p.isEmpty());
		for (Map.Entry<Object, Object> e : p.entrySet()) {
			System.out.println(e.getKey() + "=" + e.getValue());
		}
	}
}
