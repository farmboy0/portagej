package portage;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class AtomTest {

	@Test
	void testBaseAtom() {
		Atom atom1 = new Atom("sys-apps/sed");
		assertEquals("sys-apps", atom1.getCategory());
		assertEquals("sed", atom1.getPackageName());
		Atom atom2 = new Atom("sys-libs/zlib");
		assertEquals("sys-libs", atom2.getCategory());
		assertEquals("zlib", atom2.getPackageName());
		Atom atom3 = new Atom("net-misc/dhcp");
		assertEquals("net-misc", atom3.getCategory());
		assertEquals("dhcp", atom3.getPackageName());
	}

	@Test
	void testAtomVersions() {
		Atom atom1 = new Atom("sys-apps/sed-4.0.5");
		assertEquals("sys-apps", atom1.getCategory());
		assertEquals("sed", atom1.getPackageName());
		assertEquals("4.0.5", atom1.getPackageVersion());
		Atom atom2 = new Atom("sys-libs/zlib-1.1.4-r1");
		assertEquals("sys-libs", atom2.getCategory());
		assertEquals("zlib", atom2.getPackageName());
		assertEquals("1.1.4", atom2.getPackageVersion());
		assertEquals("r1", atom2.getPackageRevision());
		Atom atom3 = new Atom("net-misc/dhcp-3.0_p2");
		assertEquals("net-misc", atom3.getCategory());
		assertEquals("dhcp", atom3.getPackageName());
		assertEquals("3.0_p2", atom3.getPackageVersion());
	}

	@Test
	void testAtomPrefixOperators() {
		Atom atom1 = new Atom(">media-libs/libgd-1.6");
		assertEquals(">", atom1.getPrefix());
		assertEquals("media-libs", atom1.getCategory());
		assertEquals("libgd", atom1.getPackageName());
		assertEquals("1.6", atom1.getPackageVersion());
		Atom atom2 = new Atom(">=media-libs/libgd-1.6");
		assertEquals(">=", atom2.getPrefix());
		assertEquals("media-libs", atom2.getCategory());
		assertEquals("libgd", atom2.getPackageName());
		assertEquals("1.6", atom2.getPackageVersion());
		Atom atom3 = new Atom("=media-libs/libgd-1.6");
		assertEquals("=", atom3.getPrefix());
		assertEquals("media-libs", atom3.getCategory());
		assertEquals("libgd", atom3.getPackageName());
		assertEquals("1.6", atom3.getPackageVersion());
		Atom atom4 = new Atom("<=media-libs/libgd-1.6");
		assertEquals("<=", atom4.getPrefix());
		assertEquals("media-libs", atom4.getCategory());
		assertEquals("libgd", atom4.getPackageName());
		assertEquals("1.6", atom4.getPackageVersion());
		Atom atom5 = new Atom("<media-libs/libgd-1.6");
		assertEquals("<", atom5.getPrefix());
		assertEquals("media-libs", atom5.getCategory());
		assertEquals("libgd", atom5.getPackageName());
		assertEquals("1.6", atom5.getPackageVersion());
	}

	@Test
	void testExtendedAtomPrefixes() {
	}

	@Test
	void testAtomVersionWildcards() {
	}

}
