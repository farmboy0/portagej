package portage;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.stream.Stream;

import io.vavr.collection.Seq;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class DependencyParserTest {
	public static Stream<Arguments> values() {
		return Stream.of( //
			Arguments.of("python_single_target_python3_8? ( >=dev-lang/python-3.8.13:3.8 ) "
				+ "python_single_target_python3_9? ( >=dev-lang/python-3.9.12:3.9 ) "
				+ "python_single_target_python3_10? ( >=dev-lang/python-3.10.4:3.10 ) "
				+ "app-arch/brotli:=[abi_x86_32(-)?,abi_x86_64(-)?,abi_x86_x32(-)?,abi_mips_n32(-)?,abi_mips_n64(-)?,abi_mips_o32(-)?,abi_s390_32(-)?,abi_s390_64(-)?] "
				+ ">=app-arch/snappy-1.1.1[abi_x86_32(-)?,abi_x86_64(-)?,abi_x86_x32(-)?,abi_mips_n32(-)?,abi_mips_n64(-)?,abi_mips_o32(-)?,abi_s390_32(-)?,abi_s390_64(-)?] " //
				+ "media-libs/libpng:0= "
				+ "media-libs/mesa[egl(+),gles1,gles2,X?,abi_x86_32(-)?,abi_x86_64(-)?,abi_x86_x32(-)?,abi_mips_n32(-)?,abi_mips_n64(-)?,abi_mips_o32(-)?,abi_s390_32(-)?,abi_s390_64(-)?] "
				+ ">=media-libs/waffle-1.6.0-r1[egl,abi_x86_32(-)?,abi_x86_64(-)?,abi_x86_x32(-)?,abi_mips_n32(-)?,abi_mips_n64(-)?,abi_mips_o32(-)?,abi_s390_32(-)?,abi_s390_64(-)?] "
				+ "sys-libs/zlib[abi_x86_32(-)?,abi_x86_64(-)?,abi_x86_x32(-)?,abi_mips_n32(-)?,abi_mips_n64(-)?,abi_mips_o32(-)?,abi_s390_32(-)?,abi_s390_64(-)?] "
				+ "sys-process/procps:=[abi_x86_32(-)?,abi_x86_64(-)?,abi_x86_x32(-)?,abi_mips_n32(-)?,"
				+ "abi_mips_n64(-)?,abi_mips_o32(-)?,abi_s390_32(-)?,abi_s390_64(-)?] " //
				+ "X? ( x11-libs/libX11 ) " //
				+ "qt5? ( dev-qt/qtcore:5 dev-qt/qtgui:5[-gles2-only] dev-qt/qtwidgets:5[-gles2-only] )") //
		);
	}

	@ParameterizedTest
	@MethodSource("values")
	void testParsing(String deps) {
		DependencyParser parser = new DependencyParser();
		Seq<Dependency> parse = parser.parse(deps);
		assertNotNull(parse);
		assertFalse(parse.isEmpty());
		assertEquals(13, parse.size());
	}

}
